from conans import ConanFile, CMake, tools
import shutil, os

class LevelDBConan(ConanFile):
    name             = "leveldb"
    version          = "1.23"
    license          = "BSD-3-Clause"
    url              = "https://bitbucket.org/toge/conan-leveldb/"
    homepage         = "https://github.com/google/leveldb/"
    description      = "LevelDB is a fast key-value storage library written at Google that provides an ordered mapping from string keys to string values."
    settings         = "os", "compiler", "build_type", "arch"
    requires         = ["snappy/[>= 1.1.7]", "crc32c/[>= 1.1.1]@toge/stable"]
    generators       = "cmake"

    def source(self):
        zipfile = "leveldb.zip"
        tools.download("https://github.com/google/leveldb/archive/{}.zip".format(self.version), zipfile)
        tools.unzip(zipfile)
        shutil.move("leveldb-{}".format(self.version), "leveldb")
        os.unlink(zipfile)
        tools.replace_in_file("leveldb/CMakeLists.txt", "project(leveldb VERSION {}.0 LANGUAGES C CXX)".format(self.version), '''project(leveldb VERSION 1.22.0 LANGUAGES C CXX)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''' )
        tools.replace_in_file("leveldb/CMakeLists.txt", "check_library_exists(snappy snappy_compress \"\" HAVE_SNAPPY)", "set(HAVE_SNAPPY ON)")
        tools.replace_in_file("leveldb/CMakeLists.txt", "check_library_exists(crc32c crc32c_value \"\" HAVE_CRC32C)", "set(HAVE_CRC32C ON)")


    def build(self):
        cmake = CMake(self)
        cmake.definitions["LEVELDB_BUILD_TESTS"] = False
        cmake.definitions["LEVELDB_BUILD_BENCHMARKS"] = False
        cmake.configure(source_folder="leveldb")
        cmake.build()

    def package(self):
        self.copy("*.h", src="leveldb/include", dst="include", keep_path=True)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        if self.settings.os == "Linux":
            self.cpp_info.libs = ["leveldb", "pthread"]
        else:
            self.cpp_info.libs = ["leveldb"]
