#include <cstdlib>
#include <iostream>

#include "leveldb/db.h"

int main(int argc, const char** argv) {
    (void)argc, (void)argv;

    std::cout << leveldb::kMajorVersion << "." << leveldb::kMinorVersion << std::endl;

    leveldb::DB*     db;
    leveldb::Options options;

    options.create_if_missing = true;

    leveldb::Status status    = leveldb::DB::Open(options, "/tmp/testdb", &db);
    status.ok();


    return 0;
}
